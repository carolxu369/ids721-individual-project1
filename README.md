# My Personal Website

This repository contains the source code for my personal website. The site is built with [Zola](https://www.getzola.org/), a fast static site generator in a single binary with everything built-in. You can see the demo [video](https://drive.google.com/file/d/1uIj1IX5IZWEc_84erXzeaaU4-hd2LtQD/view?usp=sharing) here.

## Features

- Simple, clean, and modern design
- Homepage with a short personal introduction
- List of projects hosted on GitLab with descriptions
- Responsive design for various device sizes

## Prerequisites

Before you begin, ensure you have met the following requirements:

- You have installed the latest version of [Zola](https://www.getzola.org/documentation/getting-started/installation/).
- You have a basic understanding of HTML/CSS and the Rust programming language.
- You have installed Git if you plan to version control your site and push to GitLab.

## Quick Start

To get this website running on your local machine, follow these steps:

1. Clone the repository to your local machine:

    ```sh
    git clone https://gitlab.com/yourusername/mywebsite.git
    cd mywebsite
    ```

2. Run Zola's built-in web server:

    ```sh
    zola serve
    ```

   This command will start a local web server. You can view your website by navigating to `http://127.0.0.1:1111` in your web browser.
![web](./pic/web.png)
![site](./pic/site.png)

3. Make changes to the content and templates as needed. The server will automatically reload the page when files are modified.

## Deployment

I deploy the website through Netlify. The continuous deployment process for a Zola website on Netlify automates the tasks of building and publishing the site every time push new code to the Git repository. After push an update to the designated branch (usually `main`), Netlify detects this change and triggers a build process based on the settings provided. It runs the `zola build` command to generate the static files from Zola site source. Once the build is complete, Netlify takes the files from the specified `public/` directory and deploys them to its servers, making the updated version available to visitors immediately. This setup eliminates the need for manual builds or deploys, streamlining the process and ensuring the website is always up-to-date with latest changes.
![d1](./pic/d1.png)
![d2](./pic/d2.png)